﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Schema;

namespace Pop3
{
    public class Mail
    {
        private string OriginalMail { get; }
        private HashSet<string> Boundary { get; }
        public string Subject { get; private set; }
        public string From { get; private set; }
        public string Date { get; private set; }
        public string MessageID { get; private set; }
        public List<Content> Content { get; private set; }
        private static readonly string Base64Flag = "=?UTF-8?B?";

        private readonly Dictionary<string, Action<string, Mail>> _commands =
            new Dictionary<string, Action<string, Mail>>
            {
                {"Date:", (x, y) => y.Date = string.Join(" ", x.Split().Skip(1))},
                {"Message-ID:",(x,y)=> y.MessageID = string.Join("",x.Split().Skip(1).First().Skip(1).TakeWhile(z=>z!='>')) },
                {"Subject:", (x, y) =>
                {
                    var subject = string.Join("",x.SkipWhile(t=>t!=' ').Skip(1).TakeWhile(z => true)).Split('\t');
                    y.Subject = string.Join("", subject.Select(z => z.StartsWith(Base64Flag)
                        ? Encoding.UTF8.GetString(
                            Convert.FromBase64String(string.Join("",
                                z.Skip(Base64Flag.Length).Reverse().Skip(2).Reverse()))) : z));
                } },
                {"From:", (x,y )=>y.From = string.Join("",x.Split().SkipWhile(z=>!z.StartsWith("<")).First().Skip(1).TakeWhile(d=>d!='>'))}
            };


        public Mail(string mail)
        {

            Boundary = new HashSet<string>(mail.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries)
                .Where(x => x.ToString().StartsWith("--"))
                .Select(x => x += "\r\n")
                .ToArray());
            Content = new List<Content>();
            MailParse(mail);
            OriginalMail = mail;
            SaveMail();
        }

        private void MailParse(string mail)
        {
            var parts = mail.Split(Boundary.ToArray(),
                StringSplitOptions.RemoveEmptyEntries);

            var header = parts[0].Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Skip(1).ToArray();
            var isSubject = false;
            var subj = "";
            foreach (var line in header)
            {
                var lineStart = line.Split().First();
                isSubject = lineStart == "Subject:" || lineStart != "From:" && isSubject;
                if (isSubject)
                {
                    subj += line;
                    continue;
                }
                if (subj.Length != 0)
                {
                    _commands["Subject:"](subj, this);
                    subj = "";
                }
                if (_commands.ContainsKey(lineStart))
                    _commands[lineStart](line, this);
            }
            ContentParser(parts.Skip(1));

        }
        private void ContentParser(IEnumerable<string> parts)
        {
            foreach (var part in parts)
            {
                var type = part.Split().Skip(1).First();
                switch (type)
                {
                    case "text/plain;": TextPlainParser(part); break;
                    case "image/jpeg;": ImageParser(part); break;
                }
            }
        }

        private void TextPlainParser(string part)
        {
            var lines = part.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            Content.Add(new Content("Text/Plain",
                lines[1] == "Content-Transfer-Encoding: base64"
                    ? Convert.FromBase64String(string.Join("", lines.Skip(2)))
                    : Encoding.UTF8.GetBytes(string.Join("", lines.Skip(1)))));
        }

        private void ImageParser(string part)
        {
            var lines = part.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            var name = string.Join("", lines[0].Skip(34).Reverse().Skip(2).Reverse());
            Content.Add(new Content("Image/Jpeg", Convert.FromBase64String(string.Join("", lines.Skip(4)))));
        }

        public void SaveMail()
        {
            var directory = "Mail from " + From + " " + Date.Replace(':', '-');
            if (Directory.Exists(Directory.GetCurrentDirectory() + "/" + directory)) return;
            Directory.CreateDirectory(Directory.GetCurrentDirectory() + "/" + directory);
            File.WriteAllText(directory + "/Mail.txt", OriginalMail);
            File.WriteAllText(directory + "/Subject.txt", Subject);
            foreach (var file in Content)
            {
                File.WriteAllBytes(directory + "/" + file.Name + file.Type, file.Data);
            }
        }
    }


}