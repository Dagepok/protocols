﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Text;

namespace Pop3
{
    public class POP3 : IDisposable
    {
        private Dictionary<string, Func<object[], string>> ActForAnswers =>
            new Dictionary<string, Func<object[], string>>
            {
                {"RETR", RetrAnswer},
                {"TOP", TopAnswer},
                {"LIST",  ListAnswer},
                {"USER", _=> Reader.ReadLine()},
                {"PASS", _=> Reader.ReadLine()},
                {"STAT", _=>Reader.ReadLine() },
                {"DELE", _=>Reader.ReadLine() },
                {"QUIT",_=>Reader.ReadLine() }
            };
        public Dictionary<int, Mail> RetrievedMails { get; }
        private TcpClient Connection { get; }
        private SslStream SSlStream { get; }
        public StreamReader Reader { get; set; }


        public POP3(string user, string pass)
        {
            RetrievedMails = new Dictionary<int, Mail>();
            Connection = new TcpClient("pop.gmail.com", 995);
            SSlStream = new SslStream(Connection.GetStream());
            SSlStream.AuthenticateAsClient("pop.gmail.com");
            Reader = new StreamReader(SSlStream);
            Console.WriteLine(Reader.ReadLine());
            Auth(user, pass);
        }

        public void RetrieveMail(string msgNumber)
        {
            SendCommand("RETR", msgNumber);
        }

        public void GetStat()
        {
            SendCommand("STAT");
        }

        public void GetTop(string msgNumber, string count)
        {
            SendCommand("TOP", msgNumber, count);
        }

        public void GetList(params object[] args)
        {
            SendCommand("LIST", args);
        }

        public void SendNoop()
        {
            SendCommand("NOOP");
        }
        public void Quit()
        {
            SendCommand("QUIT");
        }


        public void Auth(string user, string pass)
        {
            SendCommand("USER", user);
            SendCommand("PASS", pass);

        }

        public void Delete(string id)
        {
            SendCommand("DELE", id);
        }

        public void SendCommand(string command, params object[] args)
        {

            SSlStream.Write(Encoding.UTF8.GetBytes(command + " " + string.Join(" ", args) + "\r\n"));
            Console.WriteLine(ActForAnswers[command](args));
        }

        public string OneLineAnswer(params object[] args)
        {
            return Reader.ReadLine();
        }

        private string ListAnswer(params object[] args)
        {
            if (args.Length == 0) return Reader.ReadLine();
            var line = Reader.ReadLine();
            var data = line;
            while (!string.Equals(line, ".\r\n"))
            {
                line = Reader.ReadLine() + "\r\n";
                data += line;
            }
            return data;
        }
        public string RetrAnswer(params object[] args)
        {
            var line = Reader.ReadLine();
            var data = line;
            while (!string.Equals(line, ".\r\n"))
            {
                line = Reader.ReadLine() + "\r\n";
                data += line;
            }
            RetrievedMails.Add(int.Parse(args[0].ToString()), new Mail(data));

            return "";
        }

        //private string GetSubject()
        //{

        //}
        public string TopAnswer(params object[] args)
        {
            var line = Reader.ReadLine();
            var data = line;
            for (var i = 0; i < int.Parse(args[1].ToString()); i++)

            {
                line = Reader.ReadLine() + "\r\n";
                data += line;
                if (string.Equals(line, ".\r\n")) return data;
            }
            return data;
        }

        public void Dispose()
        {
            SendCommand("QUIT");
            ((IDisposable)Connection)?.Dispose();
            SSlStream?.Dispose();
            Reader?.Dispose();
        }
    }
}
