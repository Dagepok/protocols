﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Pop3
{

    class Program
    {

        static void Main(string[] args)
        {
            const string user = "izyatesting@gmail.com";
            const string pass = "izyatester";
            try
            {

                using (var pop = new POP3(user, pass))
                {
                    Work(pop);
                }

            }
            catch
            {
                Console.WriteLine("Something was wrong");
            }

        }

        private static void Work(POP3 pop)
        {
            var command = "";
            while (command.ToLower() != "quit")
            {
                Console.Write("Write command: ");
                command = Console.ReadLine();
                ExecuteCommand(command, pop);
            }
        }
        private static void ExecuteCommand(string command, POP3 pop)
        {
            var com = command.Split();
            switch (com[0].ToUpper())
            {
                case "LIST": pop.GetList(com.Length == 1 ? "" : com[1]); break;

                case "RETR": pop.RetrieveMail(com[1]); break;
                case "STAT": pop.GetStat(); break;
                case "TOP": pop.GetTop(com[1], com[2]); break;
                case "NOOP": pop.SendNoop(); break;
                case "DELE": pop.Delete(com[1]); break;
                case "QUIT": pop.Quit(); break;
                case "HELP": Help(); break;
                default: Console.WriteLine("Unknownable Command"); break;
            }

        }

        private static void Help()
        {
            Console.WriteLine("Available command: \r\n" +
                              "LIST[N] N - id of message\r\n" +
                              "RETR N N - id of message\r\n" +
                              "STAT\r\n" +
                              "TOP[N][Count] N - id of message, Count - Count lines from top of message\r\n" +
                              "NOOP\r\n" +
                              "HELP\r\n" +
                              "Quit");
        }
    }
}
