﻿
using System;

namespace ProxyServer
{
    public class ItemBase
    {
        public string Source { get; }

        public ItemBase(string source)
        {
            Source = source;
        }

    }
}
