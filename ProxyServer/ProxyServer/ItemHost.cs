﻿using System;
using System.Text.RegularExpressions;

namespace ProxyServer
{
    public class ItemHost : ItemBase
    {
        /// <summary>
        /// Адрес хоста (домен)
        /// </summary>
        public string Host { get; } = String.Empty;

        /// <summary>
        /// Номер порта
        /// </summary>
        public int Port { get; set; }

        public ItemHost(string source) : base(source)
        {
            var myReg = new Regex(@"^(((?<host>.+?):(?<port>\d+?))|(?<host>.+?))$");
            var m = myReg.Match(source);
            Host = m.Groups["host"].Value;
            Port = !int.TryParse(m.Groups["port"].Value, out int port) ? 80 : port;


        }

    }
}
