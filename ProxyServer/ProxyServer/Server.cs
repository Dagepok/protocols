﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ProxyServer
{
    public class Server
    {
        private TcpListener Listener { get; }
        public Server()
        {
            Listener = new TcpListener(IPAddress.Any, 8080);
        }
        private static byte[] ReadToEnd(Socket mySocket)
        {
            var b = new byte[mySocket.ReceiveBufferSize];
            using (var m = new MemoryStream())
            {
                var len = 0;
                while (mySocket.Poll(1000000, SelectMode.SelectRead) && (len = mySocket.Receive(b, mySocket.ReceiveBufferSize, SocketFlags.None)) > 0)
                {
                    m.Write(b, 0, len);
                }
                return m.ToArray();
            }
        }
        public void Start()
        {
            Listener.Start();
            while (true)
            {
                if (!Listener.Pending()) continue;
                var t = new Thread(ExecuteRequest) { IsBackground = true };
                t.Start(Listener.AcceptSocket());
            }



        }
        private static void ExecuteRequest(object arg)
        {
            using (var myClient = (Socket)arg)
            {
                if (!myClient.Connected) return;
                var httpRequest = ReadToEnd(myClient);
                var http = new Parser(httpRequest);
                if (http.Items == null || http.Items.Count <= 0 || !http.Items.ContainsKey("Host"))
                {
                    Console.WriteLine("Получен запрос {0} байт, заголовки не найдены.", httpRequest.Length);
                }
                else
                {
                    Console.WriteLine("Получен запрос {0} байт, метод {1}, хост {2}:{3}", httpRequest.Length, http.HttpMethod, http.Host, http.Port);
                    var myIpHostEntry = Dns.GetHostEntry(http.Host);
                    //  Console.Write(http.GetSourceAsString());
                    if (myIpHostEntry?.AddressList == null || myIpHostEntry.AddressList.Length <= 0)
                    {
                        Console.WriteLine("Не удалось определить IP-адрес по хосту {0}.", http.Host);
                    }
                    else
                    {
                        var myIpEndPoint = new IPEndPoint(myIpHostEntry.AddressList[0], http.Port);
                        using (var myRerouting = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
                        {
                            myRerouting.Connect(myIpEndPoint);
                            if (myRerouting.Send(http.Source, http.Source.Length, SocketFlags.None) != http.Source.Length)
                            {
                                Console.WriteLine("При отправке данных удаленному серверу произошла ошибка...");
                            }
                            else
                            {
                                var httpResponse = ReadToEnd(myRerouting);
                                var response = new Parser(httpResponse);
                                if (http.Host == "www.e1.ru")
                                    response.RemoveAds();
                                myClient.Send(response.Source, response.Source.Length, SocketFlags.None);
                                }
                        }
                    }
                }
                myClient.Close();
            }
        }


    }
}