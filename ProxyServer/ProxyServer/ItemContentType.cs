﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ProxyServer.HTTP
{
    public class ItemContentType : ItemBase
    {
        public string Value { get; set; } = "text/plain";
        public string Charset { get; set; } 

        public ItemContentType(string source) : base(source)
        {
            if (string.IsNullOrEmpty(source)) return;
            var typeTail = source.IndexOf(";", StringComparison.Ordinal);
            if (typeTail == -1)
            {
                Value = source.Trim().ToLower();
                return;
            }
            Value = source.Substring(0, typeTail).Trim().ToLower();
            var p = source.Substring(typeTail + 1, source.Length - typeTail - 1);
            var myReg = new Regex(@"(?<key>.+?)=((""(?<value>.+?)"")|((?<value>[^\;]+)))[\;]{0,1}", RegexOptions.Singleline);
            var mc = myReg.Matches(p);
            foreach (Match m in mc)
            {
                if (m.Groups["key"].Value.Trim().ToLower() == "charset")
                {
                    Charset = m.Groups["value"].Value;
                }
            }
        }

    }
}
