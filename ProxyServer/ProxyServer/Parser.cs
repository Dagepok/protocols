﻿/*
 * Пример к статье «Разработка прокси-сервера»
 * http://kbyte.ru/ru/Programming/Articles.aspx?id=66&mode=art
 * Автор: Алексей Немиро
 * http://aleksey.nemiro.ru
 * Специально для Kbyte.Ru
 * http://kbyte.ru
 * Copyright © Aleksey S Nemiro, 2011
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ProxyServer.HTTP;

namespace ProxyServer
{
    public class Parser
    {
        private static string ADSE1 = File.ReadAllText("Ads.txt");
        public enum HttpMethods
        {
            GET,
            POST,
            CONNECT
        }

        private readonly string _httpVersion = "1.1";

        private readonly int _headersTail = -1;

        public HttpMethods HttpMethod { get; } = HttpMethods.GET;

        public string Path { get; } = string.Empty;

        public ItemCollection Items { get; }
        public byte[] Source { get; private set; }

        public string Host => !Items.ContainsKey("Host") ? string.Empty : ((ItemHost)Items["Host"]).Host;


        public int Port => !Items.ContainsKey("Host") ? 80 : ((ItemHost)Items["Host"]).Port;

        public int StatusCode { get; } = 0;
        public string StatusMessage { get; } = string.Empty;

        public Parser(byte[] source)
        {
            if (source == null || source.Length <= 0) return;
            Source = source;

            Items = new ItemCollection();
            var sourceString = GetSourceAsString();

            var httpInfo = sourceString.Substring(0, sourceString.IndexOf("\r\n", StringComparison.Ordinal));
            var myReg = new Regex(@"(?<method>.+)\s+(?<path>.+)\s+HTTP/(?<version>[\d\.]+)", RegexOptions.Multiline);
            if (myReg.IsMatch(httpInfo))
            {
                Match m = myReg.Match(httpInfo);
                switch (m.Groups["method"].Value.ToUpper())
                {
                    case "POST":
                        HttpMethod = HttpMethods.POST;
                        break;
                    case "CONNECT":
                        HttpMethod = HttpMethods.CONNECT;
                        break;
                    default:
                        HttpMethod = HttpMethods.GET;
                        break;
                }

                Path = m.Groups["path"].Value;
                _httpVersion = m.Groups["version"].Value;
                RemoveEncodings();
            }
            else
            {
                myReg = new Regex(@"HTTP/(?<version>[\d\.]+)\s+(?<status>\d+)\s*(?<msg>.*)", RegexOptions.Multiline);
                var m = myReg.Match(httpInfo);
                StatusCode = int.Parse(m.Groups["status"].Value);
                StatusMessage = m.Groups["msg"].Value;
                _httpVersion = m.Groups["version"].Value;
            }

            _headersTail = sourceString.IndexOf("\r\n\r\n", StringComparison.Ordinal);
            if (_headersTail != -1)
            {
                sourceString = sourceString.Substring(sourceString.IndexOf("\r\n") + 2, _headersTail - sourceString.IndexOf("\r\n") - 2);
            }

            myReg = new Regex(@"^(?<key>[^\x3A]+)\:\s{1}(?<value>.+)$", RegexOptions.Multiline);
            var mc = myReg.Matches(sourceString);
            foreach (Match mm in mc)
            {
                var key = mm.Groups["key"].Value;
                if (!Items.ContainsKey(key))
                {
                    Items.AddItem(key, mm.Groups["value"].Value.Trim("\r\n ".ToCharArray()));
                }
            }
        }

        public string GetSourceAsString()
        {
            var e = Encoding.UTF8;
            if (Items == null || !Items.ContainsKey("Content-Type") ||
                string.IsNullOrEmpty(((ItemContentType)Items["Content-Type"]).Charset)) return e.GetString(Source);
            e = Encoding.GetEncoding(((ItemContentType)Items["Content-Type"]).Charset);
            return e.GetString(Source);
        }

        public string GetHeadersAsString()
        {
            return Items?.ToString() ?? string.Empty;
        }
        public byte[] GetBody()
        {
            if (_headersTail == -1) return null;
            var result = new List<byte>();
            result.AddRange(Source.Skip(_headersTail + 10));
            //Buffer.BlockCopy(Source, _headersTail + 4, result, 0, result.Lenght);
            if (Items == null || !Items.ContainsKey("Content-Encoding") ||
                Items["Content-Encoding"].Source.ToLower() != "gzip") return result.ToArray();
            var myGzip = new GZipStream(new MemoryStream(result.ToArray()), CompressionMode.Decompress);
            using (var m = new MemoryStream())
            {

                var buffer = new byte[512];
                while (true)
                {
                    var len = myGzip.Read(buffer, 0, buffer.Length);
                    if (len == 0) break;
                    m.Write(buffer, 0, len);
                }
                result.AddRange(m.ToArray());
            }
            return result.ToArray();
        }
        public string GetBodyAsString()
        {
            var e = Encoding.UTF8;
            if (Items == null || !Items.ContainsKey("Content-Type") ||
                string.IsNullOrEmpty(((ItemContentType)Items["Content-Type"]).Charset)) return e.GetString(GetBody());

            e = Encoding.GetEncoding(((ItemContentType)Items["Content-Type"]).Charset);
            return e.GetString(GetBody());
        }


        public void SetStringBody(string newBody)
        {
            if (StatusCode <= 0)
            {
                throw new Exception("Можно изменять только содержимое, полученное в ответ от удаленного сервера.");
            }
            var e = Encoding.GetEncoding("windows-1251");
            string result = $"HTTP/{_httpVersion} {StatusCode} {StatusMessage}";
            foreach (var k in Items.Keys)
            {
                var itm = Items[k];
                if (!string.IsNullOrEmpty(result)) result += "\r\n";
                if (k.ToLower() == "content-length")
                {
                    result += $"{k}: {newBody.Length}";
                }
                else if (k.ToLower() == "content-encoding" && itm.Source.ToLower() == "gzip")
                {

                }
                else
                {
                    result += $"{k}: {itm.Source}";
                    if (k.ToLower() == "content-type" && !string.IsNullOrEmpty(((ItemContentType)Items["Content-Type"]).Charset))
                    {
                        e = Encoding.GetEncoding(((ItemContentType)Items["Content-Type"]).Charset);

                    }
                }
            }
            result += "\r\n\r\n";
            result += newBody;
            Source = e.GetBytes(result);
        }

        public void RemoveAds()
        {

            var content = GetBodyAsString();
            var topAds = "<BODY bgcolor=\"#ffffff\">\n";
            var adsStart = content.IndexOf(topAds, StringComparison.Ordinal);
            var adsEnds = content.IndexOf("<div height=\"1\">",
                StringComparison.Ordinal);
            
            if (adsEnds == -1 || adsStart == -1) return;
            content = content.Remove(adsStart + topAds.Length, adsEnds - adsStart - topAds.Length);
            SetStringBody(content);
            Source = Encoding.GetEncoding("windows-1251").GetBytes(content);
        }

        public void RemoveEncodings()
        {
            Source = Encoding.UTF8.GetBytes(GetSourceAsString().Replace("Accept-Encoding: gzip, deflate, sdch\r\n", "Accept-Encoding: \r\n"));
        }

    }
}
