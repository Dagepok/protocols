﻿using System;
using System.Collections.Generic;
using ProxyServer.HTTP;

namespace ProxyServer
{
    public class ItemCollection : Dictionary<string, ItemBase>
    {
        public ItemCollection() : base(StringComparer.CurrentCultureIgnoreCase) { }

        public void AddItem(string key, string source)
        {
            switch (key.Trim().ToLower())
            {
                case "host":
                    Add(key, new ItemHost(source));
                    break;

                case "content-type":
                    Add(key, new ItemContentType(source));
                    break;

                default:
                    Add(key, new ItemBase(source));
                    break;
            }
        }

        public override string ToString()
        {
            var result = "";
            foreach (var k in Keys)
            {
                var itm = this[k];
                if (!string.IsNullOrEmpty(result)) result += "\r\n";
                result += $"{k}: {itm.Source}";
            }
            return result;
        }
    }
}
