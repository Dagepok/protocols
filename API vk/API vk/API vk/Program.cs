﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_vk
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            while (true)
            {
                var command = Console.ReadLine();
                var id = "";
                switch (command.ToLower())
                {
                    case "friends":
                        Console.Write("Write id: ");
                        id = Console.ReadLine();
                        foreach (var friend in id == "" ? GetFriends() : GetFriends(id))
                        {
                            Console.WriteLine(friend);
                        }
                        break;
                    case "albums":
                        Console.Write("Write id: ");
                        id = Console.ReadLine();
                        foreach (var album in id == "" ? GetAlbums() : GetAlbums(id))
                        {
                            Console.WriteLine(album);
                        }
                        break;
                    case "quit": return;
                }
            }
        }

        private static IEnumerable<string> GetFriends(string id = "118574770")
        {
            var request = WebRequest.Create(@"https://api.vk.com/method/friends.get?user_id=" + id + @"&v=5.52\r\n");
            var reader = new JsonTextReader(new StreamReader(request.GetResponse().GetResponseStream()));

            var obj = JObject.Load(reader);
            var friendsList = obj["response"]["items"];
            foreach (var friend in friendsList)
            {
                var req = WebRequest.Create(@"https://api.vk.com/method/users.get?user_id=" + friend + @"&v=5.52\r\n");
                var read = new JsonTextReader(new StreamReader(req.GetResponse().GetResponseStream()));

                var response = JObject.Load(read)["response"];
                yield return (response[0]["first_name"] + "," + response[0]["last_name"]);
            }
        }

        private static IEnumerable<string> GetAlbums(string id = "8767")
        {
            var request =
                WebRequest.Create(@" https://api.vk.com/method/photos.getAlbums?user_id=" + id + @"&v=5.52");
            var reader = new JsonTextReader(new StreamReader(request.GetResponse().GetResponseStream()));
            var obj = JObject.Load(reader)["response"]["items"];
            foreach (var album in obj)
            {
                yield return album["title"] + ": " + album["description"];
            }
        }
    }
}
