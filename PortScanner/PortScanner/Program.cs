﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PortScanner
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Parallel.For(1, 1000, port => ScanUdp(IPAddress.Loopback.ToString(), port));

        }

        public static void ScanTcp(string host, int port)
        {
            try
            {
                using (new TcpClient(host, port))
                    Console.WriteLine(port + " open");
            }
            catch
            {
                Console.WriteLine(port + " closed");
            }

        }

        public static void ScanUdp(string host, int port)
        {
            var buffer = new byte[64];
            var icmp = new Socket(AddressFamily.InterNetwork, SocketType.Raw, ProtocolType.Icmp);
            icmp.Bind(new IPEndPoint(IPAddress.Loopback, port));
            icmp.IOControl(IOControlCode.ReceiveAll, new byte[] { 1, 0, 0, 0 }, new byte[] { 1, 0, 0, 0 });
            icmp.ReceiveTimeout = 500;
            var udpChecker = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            udpChecker.SendTo(new byte[] { 1, 3, 4 }, new IPEndPoint(IPAddress.Parse(host), port));
            EndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
            try
            {
                var bytesRead = icmp.ReceiveFrom(buffer, ref remoteEndPoint);
            }
            catch (SocketException e)
            {
                udpChecker.Close();
            }
            icmp.Close();
            if (buffer[20] == buffer[21] && buffer[21] == 3)

                Console.WriteLine("closed udp " + port);
            else
                Console.WriteLine("open udp " + port);
        }

    }
}
