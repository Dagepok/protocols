﻿using System;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace TimerLiarServer
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var server = new UdpServer();

            server.Start();
            while (true)
                server._shift = int.Parse(Console.ReadLine());
        }

    }
}
