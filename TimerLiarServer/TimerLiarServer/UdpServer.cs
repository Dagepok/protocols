﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace TimerLiarServer
{
    public class UdpServer
    {
        public int _shift = int.Parse(File.ReadAllText("shift.cfg"));
        private const int ListenPort = 123;
        public IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, ListenPort);
        private readonly UdpClient _receiver = new UdpClient(ListenPort);

        public void WriteLog(byte[] data)
        {
            Console.WriteLine($"From: {RemoteIpEndPoint.Address}:{RemoteIpEndPoint.Port}");
            Console.WriteLine($"Current time: {DateTime.Now}");
            Console.WriteLine($"Shift: {_shift}");
            Console.WriteLine($"Sended Time: {DateTime.Now.AddSeconds(_shift)}");
            Console.WriteLine($"Received data: {Encoding.UTF8.GetString(data)} \n");
        }

        public void Receiver()
        {
            while (true)
            {
                var data = _receiver.Receive(ref RemoteIpEndPoint);
                WriteLog(data);

                IPEndPoint ip = null;
                lock (RemoteIpEndPoint)
                    ip = new IPEndPoint(RemoteIpEndPoint.Address, RemoteIpEndPoint.Port);
                Sender(ip);
            }
        }

        public void Sender(IPEndPoint ipEndPoint)
        {

            var sender = new UdpClient();
            var answer = Encoding.UTF8.GetBytes(DateTime.Now.AddSeconds(_shift).ToString());

            sender.Connect(ipEndPoint);
            sender.Send(answer, answer.Length);
            sender.Close();
        }
        public void Start()
        {
            var thread = new Thread(Receiver);
            thread.Start();
        }


    }
}