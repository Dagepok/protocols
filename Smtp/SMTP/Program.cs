﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32.SafeHandles;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SMTP
{
    class Program
    {
        private static TcpClient Connection { get; set; }
        private static SslStream SSlStream { get; set; }
        public static StreamReader Reader { get; set; }

        private static void Main(string[] args)
        {
            Connection = new TcpClient("smtp.yandex.ru", 465);
            SSlStream = new SslStream(Connection.GetStream());
            SSlStream.AuthenticateAsClient("smtp.yandex.ru");
            Reader = new StreamReader(SSlStream);
            var reader = new JsonTextReader(new StreamReader(new FileStream("data.json", FileMode.Open)));

            var obj = JObject.Load(reader);
            var from = obj["From"];
            var to = obj["To"];
            var boundary = obj["Boundary"];
            var contentType = obj["Content-type"];
            var subject = obj["Subject"];
            var nameFrom = obj["FromName"];
            var nameTo = obj["ToName"];
            SSlStream.Write(Encoding.UTF8.GetBytes("EHLO ValitAza@yandex.ru\r\n"));
            SSlStream.Write(Encoding.UTF8.GetBytes("AUTH LOGIN\r\n"));
            SSlStream.Write(Encoding.UTF8.GetBytes(Convert.ToBase64String(Encoding.UTF8.GetBytes("ValitAza@yandex.ru")) + "\r\n"));
            SSlStream.Write(Encoding.UTF8.GetBytes(Convert.ToBase64String(Encoding.UTF8.GetBytes("03081997Azat")) + "\r\n"));
            SSlStream.Write(Encoding.UTF8.GetBytes("MAIL FROM: " + from + "\r\n"));
            SSlStream.Write(Encoding.UTF8.GetBytes("RCPT TO: " + to + "\r\n"));
            SSlStream.Write(Encoding.UTF8.GetBytes("DATA\r\n"));
            var data = "";
            data += "From: " + nameFrom + " " + nameFrom + " " + from + "\r\n";
            data += "To:" + nameTo + " " + to + "\r\n";
            data += "Subject: " + subject + "\r\n";
            data += "MIME-Version: 1.0\r\n";
            data += "Content-Type: " + contentType + "; boundary=" + boundary + "\r\n\r\n";
            data += "--" + boundary + "\r\n";
            data += "Content-Type: text/plain; charset=UTF-8;\r\nContent-Transfer-Encoding: base64\r\n\r\n";

            var mail = File.ReadAllText("text.txt");
            data += Convert.ToBase64String(Encoding.UTF8.GetBytes(mail)) + "\r\n\r\n";

          //  data += "--" + boundary + "--\r\n";
            data += "--" + boundary + "\r\n";

            const string ext = "jpeg";
            const string name = "jpeg1.jpeg";
            var attachmentBytes = File.ReadAllBytes(name);
            data += "Content-Type: image/" + ext + "; name=\"" + name +
                                        "\"\r\nContent-Disposition: attachment; filename=\"" + name +
                                        "\"\r\nContent-Transfer-Encoding: base64\r\n\r\n";

            data += Convert.ToBase64String(attachmentBytes);
            data += "\r\n--" + boundary + "--\r\n";
            data += ".\r\n";
            File.WriteAllText("out.txt", data);
            SSlStream.Write(Encoding.UTF8.GetBytes(data));
            while (true)
            {
                Console.WriteLine(Reader.ReadLine());
            }

            SSlStream.Write(Encoding.UTF8.GetBytes("QUIT"));
        }


    }
}

