﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace whois
{
    internal class Program
    {
        private static readonly object Locker = new object();
        private static string GetData(TcpClient con, string ipHost)
        {
            var stream = con.GetStream();
            stream.ReadTimeout = 500;
            var buffer = Encoding.ASCII.GetBytes(ipHost);
            stream.Write(buffer, 0, buffer.Length);
            Thread.Sleep(1000);
            var data = new byte[con.Available];
            var res = new List<string>();
            while (stream.Read(data, 0, data.Length) != 0)
                try
                {
                    res.Add(Encoding.ASCII.GetString(data));
                }
                catch (SocketException)
                {
                    break;
                }
            return string.Join("", res);
        }

        private static Tuple<string, string> TakeCountryAndAcFromData(string data, string hostName)
        {

            var lines = data.Split('\n');
            var acNumber = "";
            var country = "";
            try
            {
                var acLine = lines.First(x => x.StartsWith("origin:")).Split();
                acNumber = acLine[acLine.Length - 1];

                var countryLine = lines.First(x => x.StartsWith("country:")).Split();
                country = countryLine[countryLine.Length - 1];
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Can't find '" + hostName + "'." + "Try to use other 'whois' server");
                Environment.Exit(0);
            }
            return new Tuple<string, string>(country, acNumber);
        }

        private static void Work(string hostName)
        {

            var RipeIP = Dns.GetHostEntry("whois.ripe.net").AddressList[0].ToString();
            var con = new TcpClient(RipeIP, 43);
            var ipHost = hostName;

            #region  dnsRequest

            if (!Regex.IsMatch(ipHost, @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b"))
                try
                {
                    ipHost = Dns.GetHostEntry(hostName).AddressList[0].ToString();
                }
                catch (SocketException e)
                {
                    Console.WriteLine("Can't find '" + hostName + "'." + e.Message);
                    Environment.Exit(0);
                }

            #endregion

            var res = GetData(con, ipHost + Environment.NewLine);
            var tuple = TakeCountryAndAcFromData(res, hostName);

            lock (Locker)
            {
                PrintResults(hostName, ipHost, tuple.Item1, tuple.Item2);
            }
            con.Close();
        }

        private static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Seems like somebody forgot to write address. I can't work:c");
                Environment.Exit(0);
            }
            Parallel.ForEach(args, Work);
        }

        private static void PrintResults(string hostName, string ipHost, string country, string AcNumber)
        {
`
            Console.WriteLine("Some data for " + hostName);
            Console.WriteLine("IP address:   " + ipHost);
            Console.WriteLine("Country:      " + country);
            Console.WriteLine("AS number:    " + AcNumber);
            Console.WriteLine();

        }
    }
}