﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DnsClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var req = new byte[]
            {
                0x00, 0x29, 0x01, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x76, 0x6b, 0x03, 0x63,
                0x6f, 0x6d, 0x02, 0x61, 0x74, 0x04, 0x75, 0x72, 0x66, 0x75, 0x02, 0x72, 0x75, 0x00, 0x00, 0x01, 0x00,
                0x01
            };
            while (true)
            {
                var client = new UdpClient();
                var ip = new IPEndPoint(IPAddress.Loopback, 53);
                client.Connect(ip);
                client.Send(req, req.Length);
                var data = client.Receive(ref ip);
                Console.WriteLine(data);
                client.Close();
                Console.ReadKey();
            }
        }
    }
}
