﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DnsServer
{
    public class PacketInfo
    {
        public byte[] Data { get; set; }
        public int ID { get; set; }
        public int RequestCount { get; set; }
        public string Name { get; set; }
        public List<string> Ip { get; set; }
        public List<byte[]> IPv4 { get; set; }
        public List<byte[]> NS { get; set; }
        public byte RequestType { get; set; }
        public byte AnswerType { get; set; }
        public int AdditionalRRs { get; set; }
        public byte RequestClass { get; set; }
        public int AnswerRRs { get; set; }
        public int TTL { get; set; }
        [JsonProperty]
        private DateTime TimeAdded { get; set; }
        [JsonProperty]
        private int[] AnswerRRsLenghts { get; set; }
        [JsonProperty]
        private Dictionary<string, int> AnswerRRsNameLengths { get; set; }
        public bool IsOutdated => DateTime.Now > TimeAdded.AddSeconds(TTL);

        public PacketInfo()
        {

        }
        public PacketInfo(IReadOnlyList<byte> data, bool isRequest = true)
        {
            AnswerRRsNameLengths = new Dictionary<string, int>();
            NS = new List<byte[]>();
            IPv4 = new List<byte[]>();
            Ip = new List<string>();
            ID = GetIntFrom2Bytes(data[0], data[1]);
            RequestCount = GetIntFrom2Bytes(data[4], data[5]);
            AnswerRRs = GetIntFrom2Bytes(data[6], data[7]);
            AdditionalRRs = GetIntFrom2Bytes(data[10], data[11]);
            AnswerRRsLenghts = new int[AnswerRRs];
            Name = GetName(data);
            GetRequestTypeAndClass(data);
            if (isRequest) RequestPacket(data);
            else if (RequestType < 3) AnswerPacket(data);


        }

        private void AnswerPacket(IReadOnlyList<byte> data)
        {
            TimeAdded = DateTime.Now;
            var lastPos = 0;
            for (var i = 0; i < AnswerRRs; i++)
                lastPos = GetAnswer(data, i) + 6;
            for (var i = 0; i < AdditionalRRs; i++)
            {
                lastPos = GetAdditionalRRs(lastPos, data, i);
            }

        }

        private int GetAdditionalRRs(int pos, IReadOnlyList<byte> data, int i)
        {
            pos += 2;
            var type = GetIntFrom2Bytes(data[pos++], data[pos++]);
            if (type != 1) return pos;
            pos += 8;
            var ip = new[] { data[pos++], data[pos++], data[pos++], data[pos++] };
            Ip.Add(string.Join(".", ip));
            IPv4.Add(ip);
            return pos;
        }
        private void RequestPacket(IReadOnlyList<byte> data)
        {
            Data = data.ToArray();
        }

        private int GetAnswer(IReadOnlyList<byte> data, int i)
        {
            if (RequestType == 1) return GetAnswerA(data, i);
            if (RequestType == 2) return GetAnswerNS(data, i);

            return -1;

        }

        private int GetAnswerNS(IReadOnlyList<byte> data, int i)
        {
            var que = 19 + Name.Length;
            var shift = 0;
            for (var j = 0; j < i; j++)

                shift += 12 + AnswerRRsLenghts[j];

            var currentPos = que + shift;
            AnswerType = (byte)GetIntFrom2Bytes(data[currentPos], data[currentPos + 1]);
            currentPos += 4;
            TTL = data[currentPos] * 256 * 256 * 256 + data[currentPos + 1] * 256 * 256 + data[currentPos + 2] * 256 +
                  data[currentPos + 3];

            AnswerRRsLenghts[i] = data[currentPos + 4] * 256 + data[currentPos + 5];

            if (AnswerType == 2)
            {
                NS.Add(GetNS(data, currentPos + 6, AnswerRRsLenghts[i]));
                AnswerRRsNameLengths.Add(string.Join("", NS[i]), NS[i].Length);
            }
            return currentPos  + AnswerRRsNameLengths[string.Join("", NS[i])];
        }




        private int GetAnswerA(IReadOnlyList<byte> data, int i)
        {
            var shift = 0;
            for (var j = 0; j < i; j++)
                shift += 12 + AnswerRRsLenghts[j];
            var currentPos = 19 + Name.Length + shift;
            AnswerType = (byte)GetIntFrom2Bytes(data[currentPos], data[currentPos + 1]);
            currentPos += 4;
            TTL = data[currentPos] * 256 * 256 * 256 + data[currentPos + 1] * 256 * 256 + data[currentPos + 2] * 256 +
                  data[currentPos + 3];

            AnswerRRsLenghts[i] = data[currentPos + 4] * 256 + data[currentPos + 5];
            var ip = new[] { data[currentPos + 6], data[currentPos + 7], data[currentPos + 8], data[currentPos + 9] };

            if (AnswerType == 1)
            {
                Ip.Add(string.Join(".", ip));
                IPv4.Add(ip);
            }
            return currentPos + 10;
        }
        internal byte[] GetNS(IReadOnlyList<byte> data, int i, int lenght)
        {
            var name = new List<byte>();
            while (lenght-- > 0)
            {
                if (data[i] == 0xc0)
                {
                    name.Add(data[i++]);
                    name.Add(data[i]);
                    break;
                }
                name.Add(data[i]);
                for (var count = data[i++]; count > 0; count--)
                {
                    name.Add(data[i++]);
                    lenght--;
                }
            }
            return name.ToArray();
        }


        internal string GetName(IReadOnlyList<byte> data, int i = 12)
        {
            var name = new List<byte>();
            while (data[i] != 0)
            {
                if (data[i] == 0xc0)
                {
                    name.AddRange(Encoding.UTF8.GetBytes(GetName(data, data[++i])));
                    break;
                }
                for (var count = data[i++]; count > 0; count--)
                {
                    name.Add(data[i++]);
                }
                name.Add(46);
            }
            return Encoding.UTF8.GetString(name.ToArray());
        }

        internal void GetRequestTypeAndClass(IReadOnlyList<byte> data)
        {
            var startByteNumber = 12 + Name.Length + 1;
            RequestType = (byte)GetIntFrom2Bytes(data[startByteNumber], data[startByteNumber + 1]);
            RequestClass = (byte)GetIntFrom2Bytes(data[startByteNumber + 2], data[startByteNumber + 3]);
        }

        private static int GetIntFrom2Bytes(byte first, byte second)
        {
            var number = 0;
            var dbits = new BitArray(new[] { second, first });

            for (var i = 0; i < dbits.Count; i++)
            {
                if (dbits[i])
                    number += (int)Math.Pow(2, i);
            }
            return number;
        }

        public byte[] GeneratePacketToSend(int type, int id)
        {
            var answer = new List<byte>();
            var idbytes = Get2Bytes(id);

            var rrsBytes = Get2Bytes(AnswerRRs);
            answer.AddRange(new byte[] { idbytes[0], idbytes[1], 0x81, 0x80, 0x00, 0x01, rrsBytes[0], rrsBytes[1], 0x00, 0x00, 0x00, 0x00 });
            var name = Name.Split('.');
            foreach (var part in name)
            {
                answer.Add((byte)part.Length);
                answer.AddRange(Encoding.UTF8.GetBytes(part));
            }
            answer.AddRange(new byte[] { 0, RequestType, 0, RequestClass });

            switch (type)
            {
                case 1:

                    foreach (var ip in IPv4)
                        GenerateA(this, answer, ip);
                    break;
                case 2:
                    for (var i = 0; i < AnswerRRs; i++)
                        GenerateNS(this, answer);
                    break;
            }
            answer.ToString();
            return answer.ToArray();
        }

        private static void GenerateA(PacketInfo packetInfo, List<byte> answer, byte[] ip)
        {
            answer.Add(0xc0);
            answer.Add(0x0c);
            answer.AddRange(Get2Bytes(1));
            answer.AddRange(Get2Bytes(1));
            answer.AddRange(Get4Bytes(packetInfo.TTL));
            answer.Add(0x00);
            answer.Add(0x04);
            answer.AddRange(ip);
        }


        private static IEnumerable<byte> GetIPv4Bytes(string ip)
        {
            var bytes = Encoding.UTF8.GetBytes(ip);
            return bytes;
        }
        private static byte[] Get2Bytes(int number)
        {
            var second = (byte)(number % 256);
            var first = (byte)(number / 256);
            var bytes = new[] { first, second };
            return bytes;
        }

        private static byte[] Get4Bytes(int number)
        {
            var bytes = new List<byte>();
            for (var i = 0; i < 4; i++)
            {
                bytes.Add((byte)(number % 256));
                number /= 256;
            }
            bytes.Reverse();
            return bytes.ToArray();
        }

        private static void GenerateNS(PacketInfo packet, List<byte> answer)
        {
            for (var i = 0; i < packet.AnswerRRs; i++)
            {
                answer.Add(0xc0);
                answer.Add(0x0c);
                answer.AddRange(Get2Bytes(2));
                answer.AddRange(Get2Bytes(1));
                answer.AddRange(Get4Bytes(packet.TTL));
                answer.AddRange(Get2Bytes(packet.AnswerRRsNameLengths[string.Join("", packet.NS[i])]));
                for (var j = 0; j < packet.NS[i].Length; j++)
                    answer.Add(packet.NS[i][j]);
            }

        }

        public PacketInfo Concat(PacketInfo other)
        {
            var newPacket = new PacketInfo();
            if (Ip.Count > other.Ip.Count)
            {

                newPacket.IPv4 = IPv4;
                newPacket.Ip = Ip;
            }
            else
            {
                newPacket.IPv4 = other.IPv4;
                newPacket.Ip = other.Ip;
            }
            newPacket.NS = NS.Count > other.NS.Count ? NS : other.NS;
            newPacket.TTL = TTL > other.TTL ? other.TTL : TTL;

            newPacket.AnswerRRs = newPacket.Ip.Count + newPacket.NS.Count;
            newPacket.TimeAdded = TimeAdded > other.TimeAdded ? other.TimeAdded : TimeAdded;
            newPacket.AnswerRRsNameLengths = AnswerRRsNameLengths.Concat(other.AnswerRRsNameLengths).ToDictionary(x => x.Key, x => x.Value);
            return newPacket;
        }
    }
}