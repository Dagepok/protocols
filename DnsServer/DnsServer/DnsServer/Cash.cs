﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace DnsServer
{
    public class Cash : IDisposable
    {
        [JsonProperty]
        private const int CheckPeriodTime = 60000;
        [JsonProperty]
        private Dictionary<string, PacketInfo> FromIP { get; }
        [JsonProperty]
        private Dictionary<string, PacketInfo> FromDomain { get; }

        private readonly AutoResetEvent _autoEvent = new AutoResetEvent(true);
        private Timer Timer { get; }

        public Cash()
        {
            Timer = new Timer(CheckTTL, _autoEvent, 0, CheckPeriodTime);
            FromIP = new Dictionary<string, PacketInfo>();
            FromDomain = new Dictionary<string, PacketInfo>();
        }

        public bool Contains(PacketInfo packet)
        {
            switch (packet.RequestType)
            {
                case 1:
                    return CheckIpV4(packet);
                //case 12:
                //    return packet.IPv4.Exists(x => FromIP.ContainsKey(x));
                case 2:
                    return packet.Ip.Exists(x => FromIP.ContainsKey(x) && FromIP[x].NS.Count != 0) || FromDomain.ContainsKey(packet.Name) && FromDomain[packet.Name].NS.Count != 0;
                default:
                    return false;
            }

        }

        private bool CheckIpV4(PacketInfo packet)
        {
            try
            {
                return FromDomain[packet.Name].IPv4.Count != 0;
            }
            catch (KeyNotFoundException)
            {
                return false;
            }
        }

        public byte[] GetBytes(PacketInfo packet)
        {
            {
                switch (packet.RequestType)
                {
                    case 1:
                        return FromDomain[packet.Name].GeneratePacketToSend(packet.RequestType, packet.ID);

                    case 12:
                        //if (packet.IPv4.Exists(x => x == "127.0.0.1"))
                        //    return new byte[] { 0, (byte)packet.ID, 0x01, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x31, 0x01, 0x30, 0x01, 0x30, 0x03, 0x31, 0x32, 0x37, 0x07,
                        //        0x69, 0x6e, 0x2d, 0x61, 0x64, 0x64, 0x72, 0x04, 0x61, 0x72, 0x70, 0x61, 0x00, 0x00, 0x0c, 0x00, 0x01, 0xc0, 0x0c, 0x00, 0x0c, 0x00, 0x01, 0x00, 0x01, 0x51, 0x7f,
                        //        0x00, 26, 13, 0x64, 0x61, 0x67, 0x65, 0x70, 0x6f, 0x6b, 0x6c, 0x61, 0x70, 0x74, 0x6f, 0x70, 0x02, 0x61, 0x74, 0x04, 0x75, 0x72, 0x66, 0x75, 0x02, 0x72, 0x75, 0x00 };
                        return
                            FromIP[packet.Ip[0]].GeneratePacketToSend(packet.RequestType, packet.ID);
                    case 2:
                        {
                            return FromDomain[packet.Name].GeneratePacketToSend(packet.RequestType, packet.ID);
                        }
                    default: throw new DataException();
                }
            }
        }
        public void Add(PacketInfo packet)
        {
            if (packet.AnswerType > 2) return;
            foreach (var ip in packet.Ip)
            {
                lock (FromIP)
                    if (!FromIP.ContainsKey(ip))
                        FromIP.Add(ip, packet);
            }
            lock (FromDomain)
                if (!FromDomain.ContainsKey(packet.Name))
                    FromDomain.Add(packet.Name, packet);
        }

        private void CheckTTL(object stateInfo)
        {
            Console.WriteLine(DateTime.Now);
            lock (FromIP)
                lock (FromDomain)
                {
                    // var copy = new Dictionary<string, PacketInfo>();
                    foreach (var packet in FromIP.ToDictionary(x => x.Key, x => x.Value))
                    {
                        if (!packet.Value.IsOutdated) continue;
                        FromIP.Remove(packet.Key);
                        FromDomain.Remove(packet.Value.Name);
                    }
                }
        }

        public static void Serialize(Cash cash)
        {
            var json = JsonConvert.SerializeObject(cash, Formatting.Indented, new CashSerializer(typeof(Cash)));
            File.WriteAllText("Cash.json", json);
        }

        public void Dispose()
        {
            Serialize(this);
            _autoEvent?.Dispose();
            Timer?.Dispose();
        }
    }
}