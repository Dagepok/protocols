﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DnsServer
{
    internal class UdpListener : UdpBase
    {
        public UdpListener()
        {
            var listenOn = new IPEndPoint(IPAddress.Any, 53);
            Client = new UdpClient(listenOn);
        }

        public void Reply(byte[] data, IPEndPoint endpoint)
        {
            Client.Send(data, data.Length, endpoint);
            Console.WriteLine("Answer sended: " + endpoint);
        }

    }

    public class Server : IDisposable
    {
        private Cash Cash { get; }

        public Server()
        {
            var json = File.ReadAllText("Cash.json");
            Cash = json.Length == 0 ? new Cash() : JsonConvert.DeserializeObject<Cash>(json, new CashSerializer(typeof(Cash)));

            using (Cash)
                Start();
        }
        public void Start()
        {

            var server = new UdpListener();

            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    var received = await server.Receive();
                    var packet = new PacketInfo(received.Data);
                    var answer = Cash.Contains(packet) ? Cash.GetBytes(packet) : GetNewData(packet);
                    server.Reply(answer, received.Sender);
                }
            });
            string read;
            do
            {
                read = Console.ReadLine();
                if (read == "ser")
                    Cash.Serialize(Cash);
            } while (read != "quit");
            Environment.Exit(0);
        }

        private byte[] GetNewData(PacketInfo request)
        {
            Console.WriteLine(request.Name + " from server");
            var sender = new UdpClient();
            var server = new IPEndPoint(new IPAddress(new byte[] { 212, 193, 163, 6 }), 53);
            sender.Connect(server);
            sender.Send(request.Data, request.Data.Length);
            var data = sender.Receive(ref server);
            sender.Close();
            Cash.Add(new PacketInfo(data, false));
            return data;
        }

        public void Dispose()
        {
            Cash?.Dispose();
        }
    }

    internal struct Received
    {
        public IPEndPoint Sender;
        public byte[] Data;
    }

    internal abstract class UdpBase
    {
        protected UdpClient Client;

        protected UdpBase()
        {
            Client = new UdpClient();
        }

        public async Task<Received> Receive()
        {
            var result = await Client.ReceiveAsync();
            Console.WriteLine("Request received: " + result.RemoteEndPoint);
            return new Received
            {
                Data = result.Buffer,
                Sender = result.RemoteEndPoint
            };
        }
    }
}