﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace DnsServer
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var a = new Server();

            using (a)
               {
                a.Start();  
            }
        }
    }
}
// 02 129 128 0 1 0 2 0 0 0 0 2 118 107 3 99 111 109 0 0 1 0 1 192 12 0 1 0 1 0 0 2 126 0 4 95 213 11 180 192 12 0 1 0 1 0 0 2 126 0 4 87 240 165 82
// 02, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 2, 118, 107, 3, 99, 111, 109, 0, 0, 1, 0, 1